//region vue
declare module '*.vue'{
	import {DefineComponent} from 'vue';
	// eslint-disable-next-line @typescript-eslint/no-explicit-any, @typescript-eslint/ban-types
	const component:DefineComponent<{},{},any>;
	export default component;
}
//endregion
//region CSS modules
type CSSModuleClasses = {readonly [key:string]:string};

declare module '*.module.css'{
	const classes:CSSModuleClasses;
	export default classes;
}
declare module '*.module.scss'{
	const classes:CSSModuleClasses;
	export default classes;
}
//endregion
//region CSS
declare module '*.css' {}
declare module '*.scss' {}
//endregion
//region 图片
declare module '*.png'{
	const src:string;
	export default src;
}
declare module '*.jpg'{
	const src:string;
	export default src;
}
declare module '*.jpeg'{
	const src:string;
	export default src;
}
declare module '*.gif'{
	const src:string;
	export default src;
}
declare module '*.webp'{
	const src:string;
	export default src;
}
//endregion
//region 视频
declare module '*.mp4'{
	const src:string;
	export default src;
}
//endregion
//region 音频
declare module '*.mp3'{
	const src:string;
	export default src;
}
//endregion
//region 资源
declare module '*?resource'{
	const src:string;
	export default src;
}
declare module '*?source'{
	const src:string;
	export default src;
}
declare module '*?inline'{
	const src:string;
	export default src;
}
//endregion