## [3.13.6](https://gitee.com/tangguoxiong/powerful-cli/compare/v3.13.5...v3.13.6) (2025-02-26)


### Features

* **配置:** 新增assetsDir设置资源子目录 ([8eb695a](https://gitee.com/tangguoxiong/powerful-cli/commits/8eb695a3169775dd90f2ed4a92fa0d740154abad))



## [3.13.5](https://gitee.com/tangguoxiong/powerful-cli/compare/v3.13.4...v3.13.5) (2025-02-26)


### Features

* **配置:** alias支持设置false忽略模块 ([6750616](https://gitee.com/tangguoxiong/powerful-cli/commits/675061662d356c0b2d2978225189c0ecffb1ac85))



## [3.13.4](https://gitee.com/tangguoxiong/powerful-cli/compare/v3.13.3...v3.13.4) (2025-02-06)


### Bug Fixes

* sass使用legacy API, modern API会报错:Module loop: this module is already being loaded ([e3a51e6](https://gitee.com/tangguoxiong/powerful-cli/commits/e3a51e653d277fa451eeda25b8b3c8dfb464dd69))



## [3.13.3](https://gitee.com/tangguoxiong/powerful-cli/compare/v3.13.2...v3.13.3) (2025-01-23)


### Bug Fixes

* 锁定@swc/core版本, 新版本容易报错 ([1a60592](https://gitee.com/tangguoxiong/powerful-cli/commits/1a60592ad70ea8b23ff0a8f0117f4419675d16be))



## [3.13.2](https://gitee.com/tangguoxiong/powerful-cli/compare/v3.13.1...v3.13.2) (2025-01-23)


### Features

* **配置:** 新增unique配置, 设置项目的唯一键 ([7dc7760](https://gitee.com/tangguoxiong/powerful-cli/commits/7dc776037baf7bfbeccbec0a75c61d8d9f019fc5))



## [3.13.1](https://gitee.com/tangguoxiong/powerful-cli/compare/v3.13.0...v3.13.1) (2024-11-30)


### Features

* 支持引入mp4, mp3资源 ([04a0031](https://gitee.com/tangguoxiong/powerful-cli/commits/04a0031842aa10f682ce148d4a4cbdac27e5d5a1))



# [3.13.0](https://gitee.com/tangguoxiong/powerful-cli/compare/v3.12.6...v3.13.0) (2024-11-24)


### Bug Fixes

* **声明文件:** 类型参数是可选的 ([456f789](https://gitee.com/tangguoxiong/powerful-cli/commits/456f7896b93d5277c3d7821159c89a5e899eefbe))


### Features

* **配置:** 新增babel.extraPolyfills ([33bc66b](https://gitee.com/tangguoxiong/powerful-cli/commits/33bc66b12ef82bdea08a3b7a1d63450d1973d522))
* **配置:** 新增libConfig.polyfillAll ([2c19c49](https://gitee.com/tangguoxiong/powerful-cli/commits/2c19c498b5c11233bd142f0ccbc001835e549b45))



## [3.12.6](https://gitee.com/tangguoxiong/powerful-cli/compare/v3.12.5...v3.12.6) (2024-07-17)


### Features

* **配置:** 新增 typescript.configFile配置, 设置tsconfig.json路径 ([c4a9e0d](https://gitee.com/tangguoxiong/powerful-cli/commits/c4a9e0db6e842f04382b38fd5e7cd3d35486d5aa))



## [3.12.5](https://gitee.com/tangguoxiong/powerful-cli/compare/v3.12.4...v3.12.5) (2024-07-05)



## [3.12.4](https://gitee.com/tangguoxiong/powerful-cli/compare/v3.12.3...v3.12.4) (2024-07-05)


### Features

* **配置及运行:** 修改 htmlTemplate.favicon类型, 现在favicon可以设置为空表示不生成相应节点 ([9af556f](https://gitee.com/tangguoxiong/powerful-cli/commits/9af556fb59a425e5d60742e62c26c09501d943de))
* **配置:** 新增 pages.base、pages.meta、libConfig.html.base、libConfig.html.meta配置 ([f6a6b67](https://gitee.com/tangguoxiong/powerful-cli/commits/f6a6b6759634209f42ff7e3b77588e343ce06163))



## [3.12.3](https://gitee.com/tangguoxiong/powerful-cli/compare/v3.12.2...v3.12.3) (2024-07-05)


### Bug Fixes

* **运行:** lib命令不再生成config.libConfig.name子文件夹 ([02e1f84](https://gitee.com/tangguoxiong/powerful-cli/commits/02e1f84bd45dd9998d1e886a0a6f25435b989404))


### Features

* **配置:** 新增 pages.publicPath、libConfig.html.publicPath配置 ([8725d64](https://gitee.com/tangguoxiong/powerful-cli/commits/8725d6478c7049f20e7aa41d2008e512db26e8d5))
* **运行:** build命令区分chunk的css和普通css ([12060ff](https://gitee.com/tangguoxiong/powerful-cli/commits/12060ff06803ae037ce64b84fc67d0680a658d80))



## [3.12.2](https://gitee.com/tangguoxiong/powerful-cli/compare/v3.12.1...v3.12.2) (2024-07-01)


### Features

* **配置:** 新增 pages.templateContent、libConfig.html.templateContent配置 ([6edaf29](https://gitee.com/tangguoxiong/powerful-cli/commits/6edaf296e747c1ebbaa8c1886e90b7fcd2ab00ab))
* **配置:** copy.dir.to修改类型定义 ([84038d0](https://gitee.com/tangguoxiong/powerful-cli/commits/84038d0eefc1d3091096ce108ea13e7f515a068e))
* **配置:** htmlTemplate.templateParameters、pages.templateParameters、libConfig.html.templateParameters类型支持函数 ([c7614ba](https://gitee.com/tangguoxiong/powerful-cli/commits/c7614ba5a561cee6531dca7acf50e839e51ca17b))



## [3.12.1](https://gitee.com/tangguoxiong/powerful-cli/compare/v3.12.0...v3.12.1) (2024-06-25)


### Features

* **运行:** extraEnv新增branch ([5311c16](https://gitee.com/tangguoxiong/powerful-cli/commits/5311c16a08e4d69d6dfbcd9130151ca7c9fc4453))



# [3.12.0](https://gitee.com/tangguoxiong/powerful-cli/compare/v3.10.2...v3.12.0) (2024-06-24)


### Bug Fixes

* **配置:** : 完善注释, libConfig.template配置在文件不存在时切换为默认值 ([ae5a775](https://gitee.com/tangguoxiong/powerful-cli/commits/ae5a77536cfffb6e56516207a4ff274803ca7482))
* **运行:** copy.dir.filter, copy.filter, copy.dir.transformer, copy.transformer为数组类型时未处理元素为异步函数的情况 ([d61d62a](https://gitee.com/tangguoxiong/powerful-cli/commits/d61d62a3c7d2785bab2ea75f935207293770ea74))
* **运行:** lib命令禁用"异步chunk", 保证生成资源与config.libConfig.name解耦 ([a5cc89e](https://gitee.com/tangguoxiong/powerful-cli/commits/a5cc89e25c627516a8a746004f9634c1f92693d8))
* **运行:** lib命令生成资源与config.libConfig.name文件夹解耦 ([096a828](https://gitee.com/tangguoxiong/powerful-cli/commits/096a828d2e9fea33fd1f8a4b48da176bbe2bd159))


### Code Refactoring

* **配置:** 重构libConfig配置 ([52041b7](https://gitee.com/tangguoxiong/powerful-cli/commits/52041b7452f8903399a5dd339e279c09bcf8a16f))


### Features

* **工具方法:** 新增部分工具方法 ([f3be9cf](https://gitee.com/tangguoxiong/powerful-cli/commits/f3be9cf35747b674f8fdd554fe094d923befb7c5))
* **类型:** 优化类型导出 ([165d3e3](https://gitee.com/tangguoxiong/powerful-cli/commits/165d3e32dbfd6a06f073cb923da922babbc2b17f))
* **配置:** 新增环境工具extend ([d2b03bd](https://gitee.com/tangguoxiong/powerful-cli/commits/d2b03bd738c97acc8a96d1349cce6e6e879a31d3))
* **配置:** copy.dir.filter为函数时, 新增第三个参数, 表示全局的copy.filter. 移除copy.dir.filterInherit属性 ([c61a541](https://gitee.com/tangguoxiong/powerful-cli/commits/c61a541fb5b2a8fb589f5e24e6663bf42f2e8a48))
* **配置:** copy.dir.to为函数时, 新增第二个参数, 表示全局的copy.to ([c0254c9](https://gitee.com/tangguoxiong/powerful-cli/commits/c0254c9cae7b0f95bcdbd4dabca8c416f0a27b59))
* **配置:** copy.dir.transformer, 也可以是数组, 当为函数时, 新增第四个参数, 表示全局的copy.transformer ([e787180](https://gitee.com/tangguoxiong/powerful-cli/commits/e787180f4b05549e69b8a9e345379e086af64d64))
* **声明文件:** 模块声明 ([dee935c](https://gitee.com/tangguoxiong/powerful-cli/commits/dee935cbec06291a9fc0825a8a48439f3dc7e5a4))


### BREAKING CHANGES

* **配置:** 新增libConfig.html配置, 移除libConfig下的:template、minify、templateParameters、inject、scriptLoading
* **配置:** 移除copy.dir.filterInherit属性



## [3.10.1](https://gitee.com/tangguoxiong/powerful-cli/compare/v3.10.0...v3.10.1) (2024-04-13)


### Bug Fixes

* **配置:** CSS Modules兼容 ([f356547](https://gitee.com/tangguoxiong/powerful-cli/commits/f35654737ff453bde3a26319641c2c88a4c8c7f9))



# [3.10.0](https://gitee.com/tangguoxiong/powerful-cli/compare/v3.9.0...v3.10.0) (2024-04-13)


### Build System

* **依赖:** Node.js>=18.20.1 ([ee84cec](https://gitee.com/tangguoxiong/powerful-cli/commits/ee84cece22adbb826e7c57b1b21e2a87989387bd))


### Features

* **配置:** 调整polyfills默认值 ([dd7da05](https://gitee.com/tangguoxiong/powerful-cli/commits/dd7da05f84b4886fa05a16beecf0c52017a93d39))
* **配置:** eslint-webpack-plugin默认使用flat格式配置文件 ([15314c7](https://gitee.com/tangguoxiong/powerful-cli/commits/15314c7ed119b0ef2b0c70cff5a064196bdee4c9))
* **配置:** polyfills配置支持true, 且需要输入路径前缀 ([59586a5](https://gitee.com/tangguoxiong/powerful-cli/commits/59586a5b2a0167fd5932a75903831d72ac07eb3b))


### BREAKING CHANGES

* **配置:** polyfills配置现在必须输入路径前缀
* **依赖:** Node.js>=18.20.1



# [3.9.0](https://gitee.com/tangguoxiong/powerful-cli/compare/v3.8.4...v3.9.0) (2024-02-19)


### Build System

* **依赖:** 更新依赖 ([74652fb](https://gitee.com/tangguoxiong/powerful-cli/commits/74652fb715302d84c2468758c5caaef294d82736))


### BREAKING CHANGES

* **依赖:** 由于webpack-dev-server升级, devServer配置发生变化



## [3.8.4](https://gitee.com/tangguoxiong/powerful-cli/compare/v3.8.3...v3.8.4) (2024-02-06)


### Bug Fixes

* **默认配置:** babel-loader的sourceType配置改为unambiguous,兼容老代码转义 ([2c8d282](https://gitee.com/tangguoxiong/powerful-cli/commits/2c8d2829e8e6c703b6cb92f38d0cc9fbe576b7c3))



## [3.8.3](https://gitee.com/tangguoxiong/powerful-cli/compare/v3.8.1...v3.8.3) (2024-02-05)


### Bug Fixes

* **默认配置:** 修改transpileDependencies默认配置 ([c9ed299](https://gitee.com/tangguoxiong/powerful-cli/commits/c9ed299d55c0e4266968f4026107109db81c5c3d))



## [3.8.1](https://gitee.com/tangguoxiong/powerful-cli/compare/v3.8.0...v3.8.1) (2024-02-02)


### Features

* **运行:** vue单文件热重载 ([f97c315](https://gitee.com/tangguoxiong/powerful-cli/commits/f97c315eddeb9369dfca4955c1d46b1a1737f54b))



# [3.8.0](https://gitee.com/tangguoxiong/powerful-cli/compare/v3.7.17...v3.8.0) (2024-02-02)


### Code Refactoring

* CommonJS Module写法改为ES Module写法 ([8aaffba](https://gitee.com/tangguoxiong/powerful-cli/commits/8aaffbac7af0f1ffad1debe1748c621781dc5517))


### Features

* **默认配置:** 移除不必要的转义和polyfills ([a96840a](https://gitee.com/tangguoxiong/powerful-cli/commits/a96840aec9e254ca4fe4e7aab442b44fc02ae69f))
* **配置:** 增强eslint配置, 支持传入插件全部选项 ([e5ce00a](https://gitee.com/tangguoxiong/powerful-cli/commits/e5ce00aad15dabc8dba9fde0791e5c007a300cba))
* **依赖:** babel 7.18.0以后不需要regenerator-runtime ([1d4cb5e](https://gitee.com/tangguoxiong/powerful-cli/commits/1d4cb5e2fd9a54aa8eff2ea76e180836dcb85184))
* **运行:** 读取项目中的tsconfig.json用于ts-loader和fork-ts-checker-webpack-plugin ([62cf850](https://gitee.com/tangguoxiong/powerful-cli/commits/62cf8500c7b2fa96b4acf667a76ba8993dd063b5))
* **运行:** 支持cjs, mjs等 ([e665662](https://gitee.com/tangguoxiong/powerful-cli/commits/e66566214a5acf9eb61f792ffeeebaf7b8f67caa))


### BREAKING CHANGES

* CommonJS Module写法改为ES Module写法



## [3.7.17](https://gitee.com/tangguoxiong/powerful-cli/compare/v3.7.16...v3.7.17) (2024-01-25)


### Features

* **运行:** scss和css文件支持CSS Modules(vue单文件也支持) ([cb9bde4](https://gitee.com/tangguoxiong/powerful-cli/commits/cb9bde41beaf601eb583da712c54994018520d0d))



## [3.7.16](https://gitee.com/tangguoxiong/powerful-cli/compare/v3.7.15...v3.7.16) (2024-01-22)



## [3.7.15](https://gitee.com/tangguoxiong/powerful-cli/compare/v3.7.14...v3.7.15) (2024-01-15)


### Features

* **工具方法:** 新增copyCustom方法 ([8e6720e](https://gitee.com/tangguoxiong/powerful-cli/commits/8e6720e49107547352aae7082bc5289e76619a5d))
* **配置:** 新增eslint.active配置, 是否启用eslint ([f6de95d](https://gitee.com/tangguoxiong/powerful-cli/commits/f6de95d0c100051f623d821be62e5ece2b6dc19b))



## [3.7.14](https://gitee.com/tangguoxiong/powerful-cli/compare/v3.7.13...v3.7.14) (2024-01-09)


### Features

* **配置:** 新增emitFiles配置, 生成文件 ([eebd2fc](https://gitee.com/tangguoxiong/powerful-cli/commits/eebd2fcc0bd521af3f10bea6ee2d822156fc5bd5))
* **运行:** 修改ts-loader的选项 ([9f557d7](https://gitee.com/tangguoxiong/powerful-cli/commits/9f557d777fcb7ce54d8fe823c3e8d12203ea65c6))



## [3.7.12](https://gitee.com/tangguoxiong/powerful-cli/compare/v3.7.11...v3.7.12) (2023-12-25)


### Features

* **构建信息文件:** 增加'vue版本', 'webpack版本'信息 ([178f1ff](https://gitee.com/tangguoxiong/powerful-cli/commits/178f1ff56f720add8df555d48329ae3befe124e5))
* **运行:** extraEnv新增vueVersion, webpackVersion ([02274d4](https://gitee.com/tangguoxiong/powerful-cli/commits/02274d4b35228bcf50f1be63bd9ff0afe208a168))



## [3.7.11](https://gitee.com/tangguoxiong/powerful-cli/compare/v3.7.10...v3.7.11) (2023-12-08)



## [3.7.10](https://gitee.com/tangguoxiong/powerful-cli/compare/v3.7.9...v3.7.10) (2023-11-17)


### Features

* **配置:** 新增debug模式 ([23340af](https://gitee.com/tangguoxiong/powerful-cli/commits/23340af65d4cddf561724ae205d97ca51150e9d7))
* **配置:** vConsole新增vue属性 ([c262f74](https://gitee.com/tangguoxiong/powerful-cli/commits/c262f745ea7516ceaa33edc0d68d555e3584eed7))



## [3.7.9](https://gitee.com/tangguoxiong/powerful-cli/compare/v3.7.8...v3.7.9) (2023-11-17)


### Bug Fixes

* **运行:** 启用VConsole时, 同时启用vue生产模式下的devtools ([a4fa11a](https://gitee.com/tangguoxiong/powerful-cli/commits/a4fa11a9c398f4371df0938de3934242cd0ecfa6))



## [3.7.8](https://gitee.com/tangguoxiong/powerful-cli/compare/v3.7.7...v3.7.8) (2023-11-16)


### Features

* **配置:** 新增vConsole相关配置 ([316a1e9](https://gitee.com/tangguoxiong/powerful-cli/commits/316a1e9be029ed430aec04ce768fe9953391d9d9))



## [3.7.7](https://gitee.com/tangguoxiong/powerful-cli/compare/v3.7.6...v3.7.7) (2023-10-09)


### Bug Fixes

* **配置:** sass-loader输出不压缩,解决字体图标偶发乱码 ([c06b1fd](https://gitee.com/tangguoxiong/powerful-cli/commits/c06b1fdbd7b733eff86b2c87f3d37f909aed7c61))



## [3.7.6](https://gitee.com/tangguoxiong/powerful-cli/compare/v3.7.5...v3.7.6) (2023-08-16)


### Bug Fixes

* **工具函数:** MyPromise相关的bug ([ee35e49](https://gitee.com/tangguoxiong/powerful-cli/commits/ee35e497fe4ec7cafb1473160cfb8cfaeac30e86))



## [3.7.5](https://gitee.com/tangguoxiong/powerful-cli/compare/v3.7.4...v3.7.5) (2023-07-07)


### Bug Fixes

* **webpack.config.cjs:** 把配置文件的command独立出来,防止idea执行时产生负面影响 ([e4598d7](https://gitee.com/tangguoxiong/powerful-cli/commits/e4598d7071eb0503c10802f14eca024386c6c9cb))



## [3.7.4](https://gitee.com/tangguoxiong/powerful-cli/compare/v3.7.3...v3.7.4) (2023-07-03)


### Features

* **polyfill:** 默认添加IntersectionObserver的polyfill ([1474ea6](https://gitee.com/tangguoxiong/powerful-cli/commits/1474ea6f3368e356e0c473e99efc5d2ba0a708ab))



## [3.7.3](https://gitee.com/tangguoxiong/powerful-cli/compare/v3.7.2...v3.7.3) (2023-06-20)


### Features

* 新增command环境变量, 及打包信息 ([f575e9f](https://gitee.com/tangguoxiong/powerful-cli/commits/f575e9fe6c659c2b338567816b074ae32d69765c))



## [3.7.2](https://gitee.com/tangguoxiong/powerful-cli/compare/v3.7.1...v3.7.2) (2023-06-05)


### Bug Fixes

* **声明:** 不限制继承的配置类型 ([d8fd213](https://gitee.com/tangguoxiong/powerful-cli/commits/d8fd2135c21065e19d1f299e54804e1ced073342))



## [3.7.1](https://gitee.com/tangguoxiong/powerful-cli/compare/v3.7.0...v3.7.1) (2023-06-05)


### Features

* **声明:** 支持定义额外的参数类型(用于传递) ([f2e6d10](https://gitee.com/tangguoxiong/powerful-cli/commits/f2e6d107981959810d14571049f6dd0591a8e654))



# [3.7.0](https://gitee.com/tangguoxiong/powerful-cli/compare/v3.6.3...v3.7.0) (2023-06-05)


### Code Refactoring

* **配置:** emitEntryInjectJs新增preLoad, 移除serial ([80ddc63](https://gitee.com/tangguoxiong/powerful-cli/commits/80ddc63264e140dc971079a32389a3a7c8eb4791))


### BREAKING CHANGES

* **配置:** emitEntryInjectJs移除serial



## [3.6.2](https://gitee.com/tangguoxiong/powerful-cli/compare/v3.6.1...v3.6.2) (2023-05-18)


### Features

* **配置:** 修改devServer的默认值 ([8d5c327](https://gitee.com/tangguoxiong/powerful-cli/commits/8d5c32741212c1bc712855ada3f07e6014b3332b))



## [3.6.1](https://gitee.com/tangguoxiong/powerful-cli/compare/v3.6.0...v3.6.1) (2023-05-10)


### Features

* **配置:** 预制的cacheGroups, 移入到cacheGroups默认配置 ([b65fbab](https://gitee.com/tangguoxiong/powerful-cli/commits/b65fbab08a85f05522b55449724bfa8c71a3e6bc))
* **配置:** 预制的polyfills, 移入到babel.polyfills默认配置 ([d86a65c](https://gitee.com/tangguoxiong/powerful-cli/commits/d86a65c0cf1f41f424b7d3fa6b9651cb9e85a5b2))



# [3.6.0](https://gitee.com/tangguoxiong/powerful-cli/compare/v3.5.0...v3.6.0) (2023-05-09)


### Code Refactoring

* **配置:** 调整alias规则 ([8abd044](https://gitee.com/tangguoxiong/powerful-cli/commits/8abd044b026b66809cd3932a644b6b79b311a396))


### BREAKING CHANGES

* **配置:** alias规则和原始webpack一致



# [3.5.0](https://gitee.com/tangguoxiong/powerful-cli/compare/v3.4.1...v3.5.0) (2023-05-08)


### Code Refactoring

* **配置:** 重构copy配置相关运行逻辑及类型 ([5b7de31](https://gitee.com/tangguoxiong/powerful-cli/commits/5b7de31100f23b61c110abbd7497ed56811befde))


### BREAKING CHANGES

* **配置:** 移除copy.priority配置



## [3.4.1](https://gitee.com/tangguoxiong/powerful-cli/compare/v3.4.0...v3.4.1) (2023-05-05)


### Bug Fixes

* **运行:** pages配置为字符串时, 找不到对应template, 回退使用template/index.html ([8e20129](https://gitee.com/tangguoxiong/powerful-cli/commits/8e20129e564a7c9df4b060f9dfc7a3144140ddad))



# [3.4.0](https://gitee.com/tangguoxiong/powerful-cli/compare/v3.3.5...v3.4.0) (2023-04-20)


### Code Refactoring

* **配置:** 取消默认配置, 必须显示建立配置文件 ([74f4b39](https://gitee.com/tangguoxiong/powerful-cli/commits/74f4b390cf21920bd4b5ec031f465a28587432ec))


### BREAKING CHANGES

* **配置:** 必须显示建立配置文件



## [3.3.4](https://gitee.com/tangguoxiong/powerful-cli/compare/v3.3.3...v3.3.4) (2023-02-14)


### Bug Fixes

* **运行:** copy的dir为对象时, 如果没有filter才继承全局的 ([6034616](https://gitee.com/tangguoxiong/powerful-cli/commits/6034616b4cc7e11741dd9f530d9080d6c4e61782))



## [3.3.2](https://gitee.com/tangguoxiong/powerful-cli/compare/v3.3.1...v3.3.2) (2023-02-13)


### Features

* **运行:** copy的dir为对象时, 如果没有to和toType, 则继承全局的(修改默认配置) ([22b7141](https://gitee.com/tangguoxiong/powerful-cli/commits/22b7141db10935909d48581244e61c2e61527a5a))



## [3.3.1](https://gitee.com/tangguoxiong/powerful-cli/compare/v3.3.0...v3.3.1) (2023-02-13)


### Features

* **配置:** copy新增to, toType, priority属性 ([07a02e2](https://gitee.com/tangguoxiong/powerful-cli/commits/07a02e2726aa24ea23451a8fd1160b2aca43e16e))



