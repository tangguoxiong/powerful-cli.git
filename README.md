#### 介绍
基于[Vue3.x]

封装了webpack功能的命令行接口, 提供serve、build、lib等命令, 支持vue、js、ts等...

#### 更新记录

[CHANGELOG.md]

#### 特点
* 在底层调用了webpack。
* 提供了serve、build、lib等命令, 进行在线调试、打包、导出库等操作。
* 通过powerful.config.js文件实现灵活配置。
* 提供环境变量文件。
* 支持typescript、javascript、vue单文件、css、scss等加载。
* 基于浏览器——自动注入polyfills、转义ES6代码、自动添加css前缀。
* 代码格式校验(支持typescript、javascript、vue单文件)。
* 对打包后文件生成分析图表(优化代码)。
* 集成文件操作——复制、删除、移动、新建文件夹、压缩。

#### 安装教程

```shell
npm i -g powerful-cli-command
```

#### 使用说明

1. 执行创建命令

```shell
powerful create my-project
```
2. 选择 "vue3"

[Vue3.x]:https://cn.vuejs.org/
[CHANGELOG.md]:https://gitee.com/tangguoxiong/powerful-cli/blob/master/CHANGELOG.md
