#!/usr/bin/env node
import Service from '../util/Service.js';
import parseArgv from 'minimist';

const args=parseArgv(process.argv.slice(2),{
	string:[
		'mode',
	],
	boolean:[

	],
	default:{

	},
});
new Service(process.cwd(),args).run();
