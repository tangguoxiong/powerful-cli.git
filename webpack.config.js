import Service from './util/Service.js';
const command='config';

export default new Service(process.cwd(),{_:[command]}).getConfig().then(({webpackConfig}) => {
	return webpackConfig;
}).catch(() => {
	return {};
});
