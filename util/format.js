const storageUnit=[
	'B',
	'KB',
	'MB',
	'GB',
	'TB',
];
function sizeFormat(size,threshold=1024){
	if(typeof size==='string'){
		size=Number(size);
	}
	let i=0;
	while(size>=threshold){
		size/=1024;
		++i;
	}
	return Math.round(size*100)/100+' '+storageUnit[i];
}
function timeFormat(time){
	if(typeof time==='string'){
		time=Number(time);
	}
	return Math.round(time/1000*10)/10+'秒';
}

export {
	sizeFormat,
	timeFormat,
};
