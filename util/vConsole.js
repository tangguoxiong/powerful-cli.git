import VConsole from 'vconsole';

const vConsole=new VConsole();
if(__POWERFUL_VCONSOLE_VUE__){
	import('vue-vconsole-devtools').then(({initPlugin}) => {
		initPlugin(vConsole);
	});
}
