function defineConfig(configFn,...injectFns){
	return (param) => {
		return Promise.resolve(configFn(param)).then((baseConfig) => {
			return Object.assign(baseConfig,{
				injectFns,
			});
		});
	};
}
export {
	defineConfig,
};
