import {
	join,
} from 'node:path';
import {
	extend,
} from '../util/common.js';

export default function({baseDir,packageJson,args,command,config}){
	return {
		devtool:'eval-source-map',
		devServer:extend(config.devServer,{
			static:join(baseDir,config.outputDir),
		}),
		stats:'errors-only',
	};
};
