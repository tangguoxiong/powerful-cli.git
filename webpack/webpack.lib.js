import MiniCssExtractPlugin from 'mini-css-extract-plugin';
import {
	getTerserPlugin,
} from '../util/common.js';

export default function({baseDir,packageJson,args,command,config}){
	return {
		devtool:config.libConfig.sourceMap===true?'source-map':config.libConfig.sourceMap,
		optimization:{
			minimize:config.libConfig.minimize!==false,
			minimizer:[
				getTerserPlugin(config.libConfig.minimize,config.libConfig.sourceLog,config.terserOptions),
			],
			runtimeChunk:false,
		},
		plugins:[
			//region 提取css到单独文件
			new MiniCssExtractPlugin({
				filename:'index.css',
				chunkFilename:`${config.assetsDir}/css/[name].[contenthash].css`,
			}),
			//endregion
		],
		//region 需要的外部依赖
		externals:config.libConfig.externals,
		//endregion
	};
};