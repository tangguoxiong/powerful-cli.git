import MiniCssExtractPlugin from 'mini-css-extract-plugin';
import {
	getTerserPlugin,
} from '../util/common.js';

export default function({baseDir,packageJson,args,command,config}){
	return {
		devtool:config.buildConfig.sourceMap===true?'source-map':config.buildConfig.sourceMap,
		optimization:{
			minimize:config.buildConfig.minimize!==false,
			minimizer:[
				getTerserPlugin(config.buildConfig.minimize,config.buildConfig.sourceLog,config.terserOptions),
			],
			//region js分块
			splitChunks:{
				chunks:'all',
				usedExports:true,
				cacheGroups:config.cacheGroups,
			},
			//endregion
		},
		plugins:[
			//region 提取css到单独文件
			new MiniCssExtractPlugin({
				filename:`${config.assetsDir}/css/[name].[contenthash].css`,
				chunkFilename:`${config.assetsDir}/css/chunk/[name].[contenthash].css`,
			}),
			//endregion
		]
	};
};