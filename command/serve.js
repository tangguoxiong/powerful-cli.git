import webpack from 'webpack';
import WebpackDevServer from 'webpack-dev-server';
import chalk from 'chalk';

export default function(){
	process.env.NODE_ENV='development';
	return function({webpackConfig,config}){
		const compiler=webpack(webpackConfig);
		let port;
		const server=new WebpackDevServer({
			...webpackConfig.devServer,
			onListening(devServer){
				port=devServer.server.address().port;
			}
		},compiler);
		server.start();
		compiler.hooks.done.tap('powerful-cli serve',() => {
			WebpackDevServer.internalIP('v4').then((ip) => {
				console.log(chalk.green('\n在线地址:')+`http://${ip}:${port}`);
			});
		});
	};
};
