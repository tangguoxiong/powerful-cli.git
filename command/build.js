import webpack from 'webpack';
import webpackHandler from '../util/webpackHandler.js';

export default function(){
	process.env.NODE_ENV='production';
	return function({webpackConfig,config}){
		webpack(webpackConfig,webpackHandler({
			buildConsole:config.buildConfig.buildConsole,
		}));
	};
};
